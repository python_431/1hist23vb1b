# Des applications MS Windows sous Linux ?

C'est possible grâce à WINE (WINE Is Not an Emulator) : www.winehq.org

Il est disponible sous Debian, dans votre VM :

~~~~Bash
$ sudo apt install wine
~~~~

Télécharger une application Windows : http://www.minesweeper.info/downloads/WinmineXP.html (démineur version Windows XP)

~~~~Bash
$ wine ~/Téléchargement/Winmine__XP.exe
~~~~

Examiner les messages émis par wine... Wine indique que le programme Windows
que l'on a essayé de lancer est 32 bits, il faut installer tout un tas
de bibliothèques 32 bits Linux pour qu'il puisse exécuter un binaire 
Windows 32 bits, il nous dit même exactement quelle commande Debian
exécuter :

~~~~Bash
$ sudo dpkg --add-architecture i386 
$ sudo apt-get update
$ sudo apt install wine32:i386
$ rm -rf ~/.wine # vire la conf initiale de Wine
$ wine ~/Téléchargement/Winmine__XP.exe
~~~~

