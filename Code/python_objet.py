#!/usr/bin/env python3

class Person:
    # constructeur
    def __init__(self, nom, prenom, age):
        self.nom = nom
        self.prenom = prenom
        if (isinstance(age, int) and age >= 0 and age <= 150):
            self.age = age

    def show(self):
        print(self.prenom, self.nom, self.age, "ans")

# Notion d'héritage : créer des types dérivées
class Student(Person): # hérite de Person
    def __init__(self, nom, prenom, age, classe):
        self.classe = classe # traitement spécifique
        super().__init__(nom, prenom, age) # appelle une méthode parente
    def show(self):
        super().show()
        print("   Classe :", self.classe)

# programme principal

joe = Person("Doe", "Joe", 42)
mary = Person("Poppins", "Mary", 32)

joe.show()
mary.show()

linus = Student("Torvalds", "Linus", 54, "Versailles B1B")
linus.show()

