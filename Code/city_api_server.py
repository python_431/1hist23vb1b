#!/usr/bin/env python3

from flask import Flask, jsonify, request

cities = { 'Lyon':'lyonnais', 'Paris':'parisien', 'Nantes':'nantais' }

app = Flask(__name__)

@app.route('/api/list', methods=['GET'])
def get_list():
    return jsonify(list(cities.keys()))

@app.route('/api/add', methods=['POST', 'PUT'])
def add():
    payload = request.json
    cities.update(payload)
    return "Added: {}\n".format(payload)

@app.route('/api/get', methods=['GET'])
def get_none():
    return 'Name Required: /api/get/name\n'

@app.route('/api/get/<string:city>', methods=['GET'])
def get(city):
    info = cities.get(city,'Unknown')
    return jsonify(info)

if __name__ == '__main__':
    app.run()
