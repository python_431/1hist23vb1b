import tkinter as tk
import numpy as np

class GameOfLife(tk.Tk):
    def __init__(self, rows, cols, cell_size):
        super().__init__()
        self.title("Conway's Game of Life")
        self.geometry(f"{cols * cell_size}x{rows * cell_size}")
        self.cell_size = cell_size
        self.rows = rows
        self.cols = cols
        self.cells = np.random.randint(2, size=(rows, cols)).astype(bool)

        self.canvas = tk.Canvas(self, bg="white", width=cols*cell_size, height=rows*cell_size)
        self.canvas.pack()
        self.draw_grid()
        self.draw_cells()
        self.after(1000, self.update_cells)

    def draw_grid(self):
        for x in range(0, self.cols*self.cell_size, self.cell_size):
            self.canvas.create_line(x, 0, x, self.rows*self.cell_size, fill="gray")
        for y in range(0, self.rows*self.cell_size, self.cell_size):
            self.canvas.create_line(0, y, self.cols*self.cell_size, y, fill="gray")

    def draw_cells(self):
        for y in range(self.rows):
            for x in range(self.cols):
                if self.cells[y, x]:
                    self.canvas.create_rectangle(x*self.cell_size, y*self.cell_size, (x+1)*self.cell_size, (y+1)*self.cell_size, fill="black")

    def update_cells(self):
        new_cells = np.zeros((self.rows, self.cols), dtype=bool)
        for y in range(self.rows):
            for x in range(self.cols):
                count = self.count_neighbors(y, x)
                if self.cells[y, x]:
                    new_cells[y, x] = count == 2 or count == 3
                else:
                    new_cells[y, x] = count == 3
        self.cells = new_cells
        self.canvas.delete("all")
        self.draw_grid()
        self.draw_cells()
        self.after(1, self.update_cells)

    def count_neighbors(self, y, x):
        count = 0
        for i in range(-1, 2):
            for j in range(-1, 2):
                if i == 0 and j == 0:
                    continue
                yy = (y + i) % self.rows
                xx = (x + j) % self.cols
                if self.cells[yy, xx]:
                    count += 1
        return count


if __name__ == "__main__":
    app = GameOfLife(100, 100, 20)
    app.mainloop()


