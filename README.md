Wat : https://www.destroyallsoftware.com/talks/wat

PHP : https://eev.ee/blog/2012/04/09/php-a-fractal-of-bad-design/

Perl : https://bioperl.org/articles/How_Perl_saved_human_genome.html

Un exemple d'application Web MVC (cf https://framagit.org/jpython/meta)
 Share Code : https://framagit.org/jpython/share-code-plus

RFC 1149 : https://fr.wikipedia.org/wiki/IP_over_Avian_Carriers

Septembre éternel : https://fr.wikipedia.org/wiki/Septembre_%C3%A9ternel

OS à expérimenter (dans un hyperviseur comme Virtual Box, VMWare,
qemu, etc.) un émulateur de machine ancienne
ou sur une machine physique (PC, Mac, Raspberry PI) :

Utilisés en entreprise : 

- La plupart des distributions de GNU/Linux : Debian et variantes
(Ubuntu, Mint, Kali, etc.), Red Hat et variantes (CentOS, Rocky,
Alma, Fedora), Arch, Gentoo, Alpine, SuSE/OpenSuSE, ...

- Les UNIX BSD : OpenBSD, FreeBSD, NetBSD

- Minix

- Clone libre de MS-DOS : FreeDOS

Intéressants pour la R&D ou la curiosité (liste non exhaustive) :

- Plan 9 (issu des laboratoires Bell), sa variante 9front

- ReactOS : Clone de Windows sans aide de Microsoft !
(voir aussi Wine : fait tourner des applications MS Windows
sous Linux et macOS)

- Haiku : clone de BeOS
- AROS : clone d'AmigaOS
- UZIX : sorte d'UNIX minimal pour Z80 (8bits !), GeckOS pour 6502
- SymbOS : OS multitâche graphique pour machines 8 bits (MSX, 
 Amstrad CPC, ...)
- Redox : OS écrit en langage Rust 
- MenuetOS : OS écrit en assembleur x86 (Intel/AMD)
- Temple OS... 

Expérimenter dans Virtual Box ou qemu voir sur une clef usb :

Pour Plan 9 :

9legacy : http://9legacy.org/download/9legacy.iso.bz2

Il compressé avec bzip2 : sous Linux : `bunzip2 9legacy.iso.bz`
ou (probablement) `7zip` peut le décompresser. Il faut partir
du fichier .iso !

Pour Open BSD : https://ftp.lip6.fr/pub/OpenBSD/7.4/amd64/
fichier install74.iso

Pour UTM sur Mac Mx : https://ftp.lip6.fr/pub/OpenBSD/7.4/arm64/
Comment booter une vm UTM sur le fichier install74.img ??


Pour Haiku : http://mirror.rit.edu/haiku/r1beta4/haiku-r1beta4-x86_64-anyboot.iso

Pour ReactOS : https://altushost-swe.dl.sourceforge.net/project/reactos/ReactOS/0.4.14/ReactOS-0.4.14-release-98-gcb8061c-iso.zip




