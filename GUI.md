# Interfaces graphiques

Inventeur originel : Xerox PARC (centre de recherche),
système Alto en 1973 !!!

1984 : Apple reprend l'idée dans le Macintosh et LISA

Microsoft reprend l'idée avec Windows 1, 2, 3 (etc.)

UNIX : Propriétaires : Sunview, NeWS

NeXTSTEP sur Station NeXT : qui deviendra MacOS X, mac OS et iOS

MIT X11 (X-Window System) -> transparent par rapport au réseau IP
Open Source (licence MIT) : "mecanism not policy"

Bibliothèques d'éléments graphique côté client (widget)

Athena Widget : jeu d'exemple fournit par le MIT

Motif "standard" de l'industrie non-libres (bureau CDE)

Lesstif : implémentation libre de motif

KDE : Kommon Desktop Environment, bibliothèque Qt
Gnome : Bibliothèque Gtk (GIMP Toolkit)

X11 a finit par vieillir et poser des problèmes de performance
et de sécurité, l'équipe en charge de X11 (Xorg) a lancer
il a plus de dix ans une alternative Wayland.


