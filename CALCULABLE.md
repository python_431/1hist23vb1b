
Série de vidéos : https://www.eater.net

- Comment fonctionnent physiquement les transistors
- Construction d'un CPU 8bits à partir de composants TTL
- Construction d'un ordinateur complet autour d'un
  CPU 6502 (processeur du Commodore 64, Apple 2)
- Construction d'une carte graphique

# Émulation et virtualisation

- Hercule : simulateur de Mainframe IBM zSerie
  (capable de faire tourner MVS)
- Macintosh "Classic" (pre-OSX, macOS)
  voir https://infinitemac.org/
- Windows 3.1 (et autres OS) dans un navigateur :
  https://www.pcjs.org/software/pcx86/sys/windows/3.10/
- Un autre simulateur de PC en JS : https://bellard.org/jslinux/
