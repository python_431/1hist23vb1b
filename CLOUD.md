# Demo d'une instance dans le Cloud Scaleway

J'ai créé une instance (Debian dans une machine virtuelle)
avec la console Web du fournisseur de Cloud IaaS Scaleway.

J'y créé un compte utilisateur pour chacun d'entre vous.

Une nouvelle d'accéder à un système GNU/Linux : allez-y !

À partir d'un terminal (cmd/powershell sous MS Windows, ou
Terminal.app sous macOS, ou le terminal du bureau Gnome sous
Linux)

Notez bien que le dollar ($) est là pour indiquer que le 
contexte de ces saisie est une ligne de commande (l'invite
classique sous UNIX, sous Windows l'invite est plutôt de
la forme `C:\Users\MonLogin>`

~~~~Bash
$ ssh votre_prenom@51.15.236.226
~~~~

 
