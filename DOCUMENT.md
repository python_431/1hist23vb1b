# Formats de documents

Au début : texte simple

Dans les 60s : documents enrichis typographiquement 

Premiers systèmes : roff, nroff, troff

exemple : le manuel UNIX :

le source : `zless /usr/share/man/man3/printf.3.gz` 

le rendu en terminal: `man 3 printf`

le rendu pour impression : `man -t 3 printf > printf.ps` puis visualiser dans
Gnome : `evince printf.ps`

PS ? Postscript : language créé par Adobe, langage de description de page.

Voir : `Code/tiger.ps` que l'on peut visualiser avec `evince`

Postscript a un successeur, créé par Adobe : PDF

TeX/LaTeX : créé par Donald Knuth, auteur de la "bible" de l'algorithmique
_The Art of Computer Programming_. Il crée TeX, un système de composition
typographique de très haute qualité. Leslie Lamport crée LaTeX : un système
de macro instruction au dessus de TeX.

TeX/LaTeX c'est la _norme_ de fait dans l'édition scientifique (Maths, Physique,
Chimie, Informatique, etc.)

Example : corrige.tex dans Code.

Si vous jouer avec : les paquets texlive-* sous Linux, téléchargeable sous
Windows aussi, et en ligne : https://fr.overleaf.com/ 

