# Installation de Debian GNU/Linux

- Télécharger l'image iso d'installation par le réseau
  de Debian GNU/Linux 12 _(Bookworm)_ 
- Mettez à jour Virtual Box (à cette date : 7.0)
- Nouvelle (créer une machine virtuelle)
- Nom : Debian 12
- ISO image : sélectionner le fichier debian-12.2.0-amd64-netinst.iso
  que vous avez télécharger
- Cocher "Skip unattented installation"
- Matériel : 2048 ou 4096Mio de RAM et 2 cœurs
  _Note : si c'est dans la zone rouge sur votre système dites-le moi !_
- Enable EFI
- Taille de disque : 20Gio ok, vous pouvez mettre plus si vous voulez
- Finish
- Configuration | Réseau | Changer NAT en "Accès par pont"
  (l'interface en dessous devrait être votre WiFI)
- Configuration | Système | Interface de paravirtualisation : "Héritage" ou
  "Legacy"
- Au moindre doute : appelez-moi !!!


## Installation de Debian

- Lancez la machine virtuelle (Démarrer)
- Menu : Graphical Install (souris) ou Install (texte)
- Langue : Français
- Pays : France
- Clavier : ce qui correspond au vôtre !
- La configuration automatique du réseau : si elle ne
  fonctionne pas : prévenez-moi !
- Nom de la machine : on donne habituelle un nom
  simple, en minuscule, sympa si possible : mon
  habitude c'est des noms de villes.
- Domaine : laisser vide
- Mot de passe de « root » (l'administrateur) : rootpw
  (pas sécure mais bon, entre nous ça va)
- Nom complet du nouvel utilisateur : votre prénom
- Il vous suggère un identifiant : vous pouvez le
  simplifier si celui basé sur votre prénom est un
  peu long ou compliqué, en cas de doute faites appel
  à moi
- Positionnez un mot de passe simple, ex: userpw
- Partitionnement du disque : assisté : utiliser un disque entier
- Tout dans une seule partition
- Terminer et appliquer les changements
- Confirmer
- Analyser d'autres supports : non
- Gestion de paquets : France | deb.debian.org 
- Mandataire HTTP : laisser vide
- Collecte de statistiques : non
- Sélection des logiciel : 
  Environnement de bureau Debian | GNOME, Serveur SSH, Utilitaires usuels
  Continuer... et attendre.
- Si vous voyez une question concernant GRUB (i.e. chargeur de
  démarrage) sélectionnez /dev/sda et validez

## Finalisation de l'installation 

- Activité | Saisir "term" | Cliquez sur l'icône du Terminal
- Activité | L'icône du terminal est dans le "dock" (en bas)
  Click droit | Épingler au Dash (ou ajoutez aux favoris)

Ainsi vous pourrez lancer un Terminal rapidement, ça tombe bien
c'est votre application préférée pour les années qui viennent !

- Dans le terminal devenez root, et ajoutez votre utilisateur
dans le groupe _sudo_ (votre_id est votre identifiant utilisateur).

~~~~Bash
votre_id@votre_système:~$ whoami
-> votre identifiant apparaît ici !
votre_id@votre_système:~$ su -
Mot de passe : (rien n'apparaît pendant la saisie)
root@votre_système:~# usermod -a -G sudo votre_identifiant
(si erreur : corrigez !!!)
~~~~

- Dans le menu de Virtual Box : Périphérique | Insérez l'image CD des
  additions invités

Dans le terminal nous allons installer les  pilotes Linux pour
une meilleure intégration avec Virtual Box, pour cela il nous
faut installer l'environnement de développement C de base ainsi
que dkms _(Dynamic Kernel Modules Support)_.

~~~~Bash
root@...~# mount /media/cdrom
(message qui apparaît qui dit qu'on est en lecture seule)
root@...~# apt install build-essential dkms
(confirmer)
root@...~# bash /media/cdrom/VBoxLinuxAdditions.run
... qq minutes
root@...~# reboot
~~~~

Au démarrage, activité -> lancez un Terminal !

Tester que sudo fonctionne pour nous :

~~~~Bash
votre_id@votre_système:~$ sudo whoami
Mot de passe ... : (votre mot de passe personnel, pas celui de root)
root
~~~~

Info : les utilisateurs membres du groupe "sudo" ont le droit d'utiliser
sudo pour lancer une commande en tant qu'administrateur (i.e. root : 
sudo = DO as Super User), de plus il ne demande votre mot de passe
qu'une première fois, ensuite pendant plusieurs minutes, dans un
même terminal, il ne le demande plus.

Virtual Box, menu "Périphérique" : Presse-Papier partagé | Bidirectionnel

Vous pouvez tester que cela fonctionne : sélectionner du texte dans le
Terminal, bouton droit | Copier et sous le système d'exploitation
hôte (celui qui fait tourner Virtual Box, qui est installé sur votre
ordinateur) dans un éditeur de texte (notepad, gedit, text.app) vous
devriez pouvoir coller le texte, et inversement.

Maximisez la fenêtre de Virtual Box, le bureau Gnome devrait occuper
tout l'écran.

Ces deux derniers points montrent que les pilotes Virtual Box sont
correctement installés.

